package facci.cristian_acosta.streamreader_memoriainterna;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MemoriaInterna extends AppCompatActivity implements View.OnClickListener {

    EditText cajaCedula, cajaNombres, cajaApellidos;
    TextView cajaDatos;
    Button btnLeer, btnEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memoria_interna);

        cajaCedula = findViewById(R.id.input_cedula);
        cajaNombres = findViewById(R.id.input_nombres);
        cajaApellidos = findViewById(R.id.input_apellidos);
        cajaDatos = findViewById(R.id.input_datos);

        btnLeer = findViewById(R.id.button_leer);
        btnEscribir = findViewById(R.id.button_escribir);

        btnLeer.setOnClickListener(this);
        btnEscribir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_escribir:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Activity.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString() + ", " + cajaApellidos.getText().toString() + ", " +
                            cajaNombres.getText().toString() + ";");
                    escritor.flush();
                    escritor.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("ARCHIVO MI", "Error en el archivo de escritura");
                }
                Toast.makeText(view.getContext(), "Los datos se grabaron con éxito", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button_leer:
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String[] listaPersonas = datos.split(";");
                    cajaDatos.setText("");
                    for (int i = 0; i < listaPersonas.length; i++) {
                        cajaDatos.append(listaPersonas[i] + "\n ");
                    }
                    lector.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("ARCHIVO MI", "Error en la lectura del archivo " + ex.getMessage());
                }
                break;
        }
    }
}
